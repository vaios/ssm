#include "all.h"

service *
service_new(void) {
  service *sv = ecalloc(sizeof(service), 1);
  sv->status = STOPPED;
  sv->target = STOPPED;
  sv->pid = -1;
  sv->notif_fd = -1;
  sv->out_fd = -1;
  sv->notif_fd_src = -1;
  sv->timeout_kill = 20000;
  sv->timeout_finish = 30000;
  sv->timeout_restart = 1000;
  return sv;
}

void
service_delete(service *sv, int closefds) {
  free(sv->name);
  free(sv->path);
  for (size_t i = 0; i < sv->run_nargv; i++)
    free(sv->run_argv[i]);
  free(sv->run_argv);
  free(sv->run_exe);
  for (size_t i = 0; i < sv->fin_nargv; i++)
    free(sv->fin_argv[i]);
  free(sv->fin_argv);
  free(sv->fin_exe);
  for (size_t i = 0; i < sv->ndep_names; i++)
    free(sv->dep_names[i]);
  free(sv->dep_names);
  free(sv->deps);
  free(sv->in);
  free(sv->out);
  if (closefds && sv->notif_fd_src >= 0)
    fd_close(sv->notif_fd_src);
  if (closefds && sv->out_fd >= 0)
    fd_close(sv->out_fd);
  free(sv);
}

char *
e0strdup(char *s) {
  return s ? estrdup(s) : 0;
}

void nullterm_argv(char ***argv, size_t *nargv, size_t *margv) {
  list_minsize((void **)argv, nargv, margv, sizeof(char *), *nargv + 2);
  (*argv)[*nargv] = 0;
  (*argv)[*nargv + 1] = 0;
}

service *
service_copy(service *sv) {
  service *sd = service_new();
  sd->name = e0strdup(sv->name);
  sd->path = e0strdup(sv->path);
  sd->daemon = sv->daemon;
  for (size_t i = 0; i < sv->run_nargv; i++) {
    char *s = e0strdup(sv->run_argv[i]);
    list_append((void **)&sd->run_argv, &sd->run_nargv, &sd->run_margv, sizeof(char *), &s);
  }
  nullterm_argv(&sd->run_argv, &sd->run_nargv, &sd->run_margv);
  sd->run_exe = e0strdup(sv->run_exe);
  for (size_t i = 0; i < sv->fin_nargv; i++) {
    char *s = e0strdup(sv->fin_argv[i]);
    list_append((void **)&sd->fin_argv, &sd->fin_nargv, &sd->fin_margv, sizeof(char *), &s);
  }
  nullterm_argv(&sd->fin_argv, &sd->fin_nargv, &sd->fin_margv);
  sd->fin_exe = e0strdup(sv->fin_exe);
  for (size_t i = 0; i < sv->ndep_names; i++) {
    char *s = e0strdup(sv->dep_names[i]);
    list_append((void **)&sd->dep_names, &sd->ndep_names, &sd->mdep_names, sizeof(char *), &s);
  }
  sd->in = e0strdup(sv->in);
  sd->out = e0strdup(sv->in);
  sd->notif_fd = sv->notif_fd;
  sd->timeout_kill = sv->timeout_kill;
  sd->timeout_finish = sv->timeout_finish;
  sd->timeout_restart = sv->timeout_restart;
  sd->notif_fd_src = sv->notif_fd_src;
  sd->pid = sv->pid;
  sd->status = sv->status;
  sd->target = sv->target;
  sd->not_before = sv->not_before;
  sd->last_run_start = sv->last_run_start;
  sd->last_run_stop = sv->last_run_stop;
  sd->last_available_start = sv->last_available_start;
  sd->last_available_stop = sv->last_available_stop;
  return sd;
}

void
service_ser(service *sv, char **buf, size_t *nbuf, size_t *mbuf) {
  ser_cstr(buf, nbuf, mbuf, "service");
  ser_cstr(buf, nbuf, mbuf, sv->name);
  ser_long(buf, nbuf, mbuf, sv->notif_fd_src);
  ser_long(buf, nbuf, mbuf, sv->pid);
  ser_long(buf, nbuf, mbuf, sv->status);
  ser_long(buf, nbuf, mbuf, sv->target);
  ser_timespec(buf, nbuf, mbuf, sv->not_before);
  ser_timespec(buf, nbuf, mbuf, sv->last_run_start);
  ser_timespec(buf, nbuf, mbuf, sv->last_run_stop);
  ser_long(buf, nbuf, mbuf, sv->last_run_exit_status);
  ser_timespec(buf, nbuf, mbuf, sv->last_available_start);
  ser_timespec(buf, nbuf, mbuf, sv->last_available_stop);
  ser_cstr(buf, nbuf, mbuf, sv->path);
  ser_long(buf, nbuf, mbuf, sv->daemon);
  ser_long(buf, nbuf, mbuf, sv->notif_fd);
  ser_long(buf, nbuf, mbuf, sv->timeout_kill);
  ser_long(buf, nbuf, mbuf, sv->timeout_finish);
  ser_long(buf, nbuf, mbuf, sv->timeout_restart);
  ser_cstr(buf, nbuf, mbuf, sv->run_exe);
  ser_cstrarray(buf, nbuf, mbuf, sv->run_argv, sv->run_nargv);
  ser_cstr(buf, nbuf, mbuf, sv->fin_exe);
  ser_cstrarray(buf, nbuf, mbuf, sv->fin_argv, sv->fin_nargv);
  ser_cstrarray(buf, nbuf, mbuf, sv->dep_names, sv->ndep_names);
  ser_cstr(buf, nbuf, mbuf, sv->in);
  ser_cstr(buf, nbuf, mbuf, sv->out);
}

int
service_deser(char *buf, size_t nbuf, size_t *of, service **svr) {
  service *sv = service_new();
  char *s = 0;
  long l;
  if (deser_cstr(buf, nbuf, of, &s))
    return service_delete(sv, 0), 1;
  if (strcmp(s, "service"))
    return free(s), service_delete(sv, 0), 1;
  free(s);
  if (deser_cstr(buf, nbuf, of, &sv->name))
    return service_delete(sv, 0), 1;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->notif_fd_src = l;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->pid = l;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->status = l;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->target = l;
  if (deser_timespec(buf, nbuf, of, &sv->not_before))
    return service_delete(sv, 0), 1;
  if (deser_timespec(buf, nbuf, of, &sv->last_run_start))
    return service_delete(sv, 0), 1;
  if (deser_timespec(buf, nbuf, of, &sv->last_run_stop))
    return service_delete(sv, 0), 1;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->last_run_exit_status = l;
  if (deser_timespec(buf, nbuf, of, &sv->last_available_start))
    return service_delete(sv, 0), 1;
  if (deser_timespec(buf, nbuf, of, &sv->last_available_stop))
    return service_delete(sv, 0), 1;
  if (deser_cstr(buf, nbuf, of, &sv->path))
    return service_delete(sv, 0), 1;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->daemon = l;
  if (deser_long(buf, nbuf, of, &l))
    return service_delete(sv, 0), 1;
  sv->notif_fd = l;
  if (deser_long(buf, nbuf, of, &sv->timeout_kill))
    return service_delete(sv, 0), 1;
  if (deser_long(buf, nbuf, of, &sv->timeout_finish))
    return service_delete(sv, 0), 1;
  if (deser_long(buf, nbuf, of, &sv->timeout_restart))
    return service_delete(sv, 0), 1;
  if (deser_cstr(buf, nbuf, of, &sv->run_exe))
    return service_delete(sv, 0), 1;
  if (deser_cstrarray(buf, nbuf, of, &sv->run_argv, &sv->run_nargv, &sv->run_margv))
    return service_delete(sv, 0), 1;
  if (deser_cstr(buf, nbuf, of, &sv->fin_exe))
    return service_delete(sv, 0), 1;
  if (deser_cstrarray(buf, nbuf, of, &sv->fin_argv, &sv->fin_nargv, &sv->fin_margv))
    return service_delete(sv, 0), 1;
  if (deser_cstrarray(buf, nbuf, of, &sv->dep_names, &sv->ndep_names, &sv->mdep_names))
    return service_delete(sv, 0), 1;
  if (deser_cstr(buf, nbuf, of, &sv->in))
    return service_delete(sv, 0), 1;
  if (deser_cstr(buf, nbuf, of, &sv->out))
    return service_delete(sv, 0), 1;
  *svr = sv;
  return 0;
}

int
read_exec_file(FILE *f, char **exe, char ***argv, size_t *nargv, size_t *margv) {
  if (read_linearray(f, argv, nargv, margv))
    return 1;
  if (!nargv)
    return 1;
  *exe = (*argv)[0] ? estrdup((*argv)[0]) : 0;
  if (!*exe)
    return 1;
  nullterm_argv(argv, nargv, margv);
  return 0;
}

int
cfg_file_long(char *path, char *filename, long *l) {
  char *fp = pathmcat(path, filename);
  FILE *f = fopen(fp, "r");
  free(fp);
  if (!f)
    return -1;
  int r = read_long(f, l);
  fclose(f);
  return r;
}

int
cfg_file_linearray(char *path, char *filename, char **exe, char ***argv, size_t *nargv, size_t *margv) {
  char *fp = pathmcat(path, filename);
  FILE *f = fopen(fp, "r");
  free(fp);
  if (!f)
    return -1;
  int r = exe ? read_exec_file(f, exe, argv, nargv, margv) : read_linearray(f, argv, nargv, margv);
  fclose(f);
  return r;
}

int
cfg_file_line(char *path, char *filename, char **out) {
  char **argv = 0;
  size_t nargv = 0;
  size_t margv = 0;
  int r = cfg_file_linearray(path, filename, 0, &argv, &nargv, &margv);
  if (r)
    return *out = 0, freestrarray(argv, nargv, margv), r;
  if (nargv != 1)
    return freestrarray(argv, nargv, margv), 1;
  *out = argv[0];
  argv[0] = 0;
  return freestrarray(argv, nargv, margv), 0;
}

int
cfg_file_exists(char *path, char *filename) {
  char *fp = pathmcat(path, filename);
  int r = !access(fp, F_OK);
  free(fp);
  return r;
}

void
service_reset_cfg(service *sv) {
  for (size_t i = 0; i < sv->run_nargv; i++)
    free(sv->run_argv[i]);
  free(sv->run_argv);
  sv->run_argv = 0;
  sv->run_nargv = sv->run_margv = 0;
  free(sv->run_exe);
  sv->run_exe = 0;
  for (size_t i = 0; i < sv->fin_nargv; i++)
    free(sv->fin_argv[i]);
  free(sv->fin_argv);
  sv->fin_argv = 0;
  sv->fin_nargv = sv->fin_margv = 0;
  free(sv->fin_exe);
  sv->fin_exe = 0;
  for (size_t i = 0; i < sv->ndep_names; i++)
    free(sv->dep_names[i]);
  free(sv->dep_names);
  sv->dep_names = 0;
  sv->ndep_names = sv->mdep_names = 0;
  free(sv->deps);
  sv->deps = 0;
  sv->ndeps = sv->mdeps = 0;
  free(sv->in);
  sv->in = 0;
  free(sv->out);
  sv->out = 0;
  sv->notif_fd = -1;
  sv->timeout_kill = 20000;
  sv->timeout_finish = 30000;
  sv->timeout_restart = 1000;
}

int
service_read_cfg(service *sv) {
  sv->daemon = cfg_file_exists(sv->path, "daemon");

  if (cfg_file_linearray(sv->path, "run", &sv->run_exe, &sv->run_argv, &sv->run_nargv, &sv->run_margv) > 0)
    return 1;
  if (cfg_file_linearray(sv->path, "finish", &sv->fin_exe, &sv->fin_argv, &sv->fin_nargv, &sv->fin_margv) > 0)
    return 1;

  if (cfg_file_linearray(sv->path, "dependencies", 0, &sv->dep_names, &sv->ndep_names, &sv->mdep_names) > 0)
    return 1;

  if (cfg_file_line(sv->path, "in", &sv->in) > 0)
    return 1;
  if (cfg_file_line(sv->path, "out", &sv->out) > 0)
    return 1;

  long l = -1;
  if (!cfg_file_long(sv->path, "notification-fd", &l))
    sv->notif_fd = l;

  cfg_file_long(sv->path, "timeout-kill", &sv->timeout_kill);
  cfg_file_long(sv->path, "timeout-finish", &sv->timeout_finish);
  cfg_file_long(sv->path, "timeout-restart", &sv->timeout_restart);
  return 0;
}

int
service_validate_name(char *name) {
  if (!name)
    return 1;
  size_t l = strlen(name);
  if (!l)
    return 1;
  for (size_t i = 0; i < l; i++)
    if (name[i] == '\n')
      return 1;
  return 0;
}

service *
service_read(char *path, char *name) {
  if (service_validate_name(name))
    return 0;
  if (access(path, R_OK))
    return 0;

  service *sv = service_new();
  sv->name = estrdup(name);
  sv->path = estrdup(path);
  if (service_read_cfg(sv))
    return service_delete(sv, 0), (service *)0;
  return sv;
}

int
service_waiting(service *sv) {
  return sv->status == STOPPING
    || sv->status == FINISH
    || sv->status == WAIT_TIMEOUT;
}

char *status_names[] = {
  "WAIT_DEPS",
  "WAIT_TIMEOUT",
  "UNAVAILABLE",
  "AVAILABLE",
  "STOPPING",
  "FINISH",
  "STOPPED",
  "FAILED",
};

char *
service_status_str(int status) {
  return status >= 0 && status < LEN(status_names) ? status_names[status] : "invalid";
}

void
service_print_info(service *sv) {
  printf(
      "name: %s\n"
      "path: %s\n"
      "status: %s\n"
      "target: %s\n"
      "pid: %ld\n"
      "daemon: %s\n"
      "notif_fd: %d\n"
      "notif_fd_src: %d\n",
      sv->name,
      sv->path,
      service_status_str(sv->status),
      service_status_str(sv->target),
      (long)sv->pid,
      sv->daemon ? "yes" : "no",
      sv->notif_fd,
      sv->notif_fd_src);
}

void
service_set_status(service *sv, int status) {
  clock_gettime(CLOCK_MONOTONIC, &sv->not_before);
  if (status == AVAILABLE && sv->status != AVAILABLE)
    sv->last_available_start = sv->not_before;
  if (sv->status == AVAILABLE && status != AVAILABLE)
    sv->last_available_stop = sv->not_before;

  if (status == STOPPING)
    ts_add_ms_p(&sv->not_before, sv->timeout_kill);
  if (status == FINISH)
    ts_add_ms_p(&sv->not_before, sv->timeout_finish);
  if (status == WAIT_TIMEOUT) {
    sv->not_before = sv->last_run_start;
    ts_add_ms_p(&sv->not_before, sv->timeout_restart);
  }

  logprintf(1, "%s: %s -> %s\n",
      sv->name ? sv->name : "(null)", service_status_str(sv->status), service_status_str(status));
  sv->status = status;
}

void
service_set_target(service *sv, int target) {
  struct timespec ts;
  clock_gettime(CLOCK_MONOTONIC, &ts);
  logprintf(2, "%s: target %s -> %s\n",
      sv->name ? sv->name : "(null)", service_status_str(sv->target), service_status_str(target));
  sv->target = target;
}

int
service_transition_exec_run(service *sv) {
  if (sv->pid != -1)
    return service_set_status(sv, FAILED), 1;
  clock_gettime(CLOCK_MONOTONIC, &sv->last_run_start);
  if (!sv->run_exe)
    return service_set_status(sv, AVAILABLE), 0;
  sv->pid = execf(sv->run_exe, sv->run_argv, sv->path, 1,
      sv->notif_fd, sv->notif_fd < 0 ? 0 : &sv->notif_fd_src, 1, sv->in, sv->out, &sv->out_fd);
  if (sv->pid == -1)
    return service_set_status(sv, FAILED), 1;
  service_set_status(sv, sv->daemon && sv->notif_fd < 0 ? AVAILABLE : UNAVAILABLE);
  return 0;
}

int
service_transition_exec_fin(service *sv) {
  if (sv->pid != -1)
    return service_set_status(sv, FAILED), 1;
  if (!sv->fin_exe) {
    service_set_status(sv, sv->target == STOPPED ? STOPPED : WAIT_TIMEOUT);
    return 0;
  }
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  ser_long(&buf, &nbuf, &mbuf, sv->last_run_exit_status);
  sv->fin_argv[sv->fin_nargv] = buf;
  sv->pid = execf(sv->fin_exe, sv->fin_argv, sv->path, 1,
      0, 0, 1, sv->in, sv->out, &sv->out_fd);
  sv->fin_argv[sv->fin_nargv] = 0;
  free(buf);
  if (sv->pid == -1)
    return service_set_status(sv, FAILED), 1;
  service_set_status(sv, FINISH);
  return 0;
}

ssize_t
ditch_input(int fd) {
  char buf[32];
  ssize_t r;
  while ((r = read(fd, buf, sizeof(buf))) > 0
        || (r == -1 && errno == EINTR))
    ;
  return r;
}

int
service_transition_notified(service *sv) {
  if (sv->status != UNAVAILABLE)
    return 1;
  service_set_status(sv, AVAILABLE);
  return 0;
}

int
service_fd_polled(service *sv, int revents) {
  int r = 0;
  if (sv->notif_fd_src < 0)
    return 1;
  int eof = 0;
  if (revents & POLLIN) {
    r |= service_transition_notified(sv);
    eof = !ditch_input(sv->notif_fd_src);
  }
  if (eof || revents != POLLIN)
    r |= service_close_notif_fd(sv);
  return r;
}

int
service_transition_term(service *sv) {
  if (sv->status != UNAVAILABLE && sv->status != AVAILABLE)
    return 1;
  if (sv->pid <= 0)
    return service_transition_exec_fin(sv);
  kill(sv->pid, SIGTERM);
  service_set_status(sv, STOPPING);
  return 0;
}

int
service_close_notif_fd(service *sv) {
  if (sv->notif_fd_src < 0)
    return 1;
  fd_close(sv->notif_fd_src);
  sv->notif_fd_src = -1;
  return 0;
}

int
service_close_out_fd(service *sv) {
  if (sv->out_fd < 0)
    return 1;
  fd_close(sv->out_fd);
  sv->out_fd = -1;
  return 0;
}

int
service_close_fds(service *sv) {
  return service_close_notif_fd(sv) & service_close_out_fd(sv);
}

int
service_transition_kill(service *sv) {
  if (sv->status != STOPPING && sv->status != FINISH)
    return 1;
  if (sv->pid <= 0)
    return 1;
  kill(sv->pid, SIGKILL);
  service_close_fds(sv);
  sv->pid = -1;
  sv->last_run_exit_status = 128 + SIGKILL;
  clock_gettime(CLOCK_MONOTONIC, &sv->last_run_stop);
  if (sv->status == STOPPING)
    return service_transition_exec_fin(sv);
  service_set_status(sv, sv->target == AVAILABLE ? WAIT_TIMEOUT : STOPPED);
  return 0;
}

int
service_transition_exit(service *sv, int code) {
  if (sv->status == FAILED)
    return 1;
  service_close_fds(sv);
  sv->pid = -1;
  if (!sv->daemon && !code && (sv->status == UNAVAILABLE || sv->status == AVAILABLE)) {
    if (sv->status == UNAVAILABLE)
      service_set_status(sv, AVAILABLE);
    sv->last_run_exit_status = code;
    clock_gettime(CLOCK_MONOTONIC, &sv->last_run_stop);
    return 0;
  }
  if (sv->status == UNAVAILABLE || sv->status == AVAILABLE || sv->status == STOPPING) {
    sv->last_run_exit_status = code;
    clock_gettime(CLOCK_MONOTONIC, &sv->last_run_stop);
    return service_transition_exec_fin(sv);
  }
  if (sv->status == FINISH)
    return service_set_status(sv, code == 125 ? FAILED
        : (sv->target == AVAILABLE ? WAIT_TIMEOUT : STOPPED)), 0;
  service_set_status(sv, FAILED);
  return 1;
}

int
service_deps_available(service *sv) {
  if (sv->ndeps != sv->ndep_names)
    return -1;
  for (size_t i = 0; i < sv->ndep_names; i++)
    if (sv->deps[i]->status != AVAILABLE)
      return 1;
  return 0;
}

int
checktime(struct timespec *ts) {
  struct timespec now;
  clock_gettime(CLOCK_MONOTONIC, &now);
  if (now.tv_sec < ts->tv_sec)
    return 1;
  if (now.tv_sec == ts->tv_sec
      && now.tv_nsec < ts->tv_nsec)
    return 1;
  return 0;
}

int
service_transition_stop_timeout(service *sv) {
  if (sv->status != STOPPING)
    return -1;
  if (checktime(&sv->not_before))
    return 0;
  return service_transition_kill(sv);
}

int service_transition_finish_timeout(service *sv) {
  if (sv->status != FINISH)
    return -1;
  if (checktime(&sv->not_before))
    return 0;
  return service_transition_kill(sv);
}

int
service_transition_wait_timeout(service *sv) {
  if (sv->status != WAIT_TIMEOUT)
    return -1;
  if (checktime(&sv->not_before))
    return 0;
  service_set_status(sv, WAIT_DEPS);
  return 0;
}

int service_transition_wait_deps(service *sv) {
  if (sv->status != WAIT_DEPS)
    return -1;
  if (service_deps_available(sv))
    return 1;
  return service_transition_exec_run(sv);
}

int
service_start(service *sv) {
  service_set_target(sv, AVAILABLE);
  if (sv->status == STOPPED)
    service_set_status(sv, WAIT_DEPS);
  if (sv->status == FAILED)
    return 1;
  else
    return 0;
}

int
service_stop(service *sv) {
  service_set_target(sv, STOPPED);
  if (sv->status == WAIT_TIMEOUT || sv->status == WAIT_DEPS)
    service_set_status(sv, STOPPED);
  if (sv->status == UNAVAILABLE || sv->status == AVAILABLE)
    return service_transition_term(sv);
  return 0;
}

int
service_restart(service *sv) {
  return (service_stop(sv) || service_start(sv));
}

int
service_wakeup_time(service *sv, struct timespec *ts) {
  if (sv->status != STOPPING && sv->status != FINISH && sv->status != WAIT_TIMEOUT)
    return 1;
  *ts = ts_min(*ts, sv->not_before);
  return 0;
}
