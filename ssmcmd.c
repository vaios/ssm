#include "all.h"
#include "ssm.h"
#include "ssmcmd.h"

cmd _cmds[] = {
  {"load", cmd_load},
  {"reload", cmd_reload},
  {"start", cmd_start},
  {"stop", cmd_stop},
  {"restart", cmd_restart},
  {"term", cmd_term},
  {"status", cmd_status},
  {"list", cmd_list},
};
cmd *cmds = _cmds;
size_t ncmds = LEN(_cmds);

int
mq_senderrmsg(mq *q, int err) {
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  ser_cstr(&buf, &nbuf, &mbuf, "Err");
  ser_long(&buf, &nbuf, &mbuf, err);
  mq_append_msg(q, 0, buf, nbuf, mbuf);
  return mq_send(q);
}

int
ssm_load(char *name) {
  serviceset *new = serviceset_copy(ss);
  if (!new)
    return 1;
  if (serviceset_load(new, name))
    return serviceset_delete(new, 0), 1;
  serviceset_delete(ss, 0);
  ss = new;
  return 0;
}

int
cmd_load(char *name) {
  int err = ssm_load(name);
  return mq_senderrmsg(out, err), err;
}

int
ssm_reload(char *name) {
  serviceset *new = serviceset_copy(ss);
  if (!new)
    return 1;
  if (serviceset_reload(new, name))
    return serviceset_delete(new, 0), 1;
  serviceset_delete(ss, 0);
  ss = new;
  return 0;
}

int
cmd_reload(char *name) {
  int err = ssm_reload(name);
  return mq_senderrmsg(out, err), err;
}

int
ssm_start(char *name) {
  if (serviceset_service_by_name(ss, name))
    return serviceset_start(ss, name);
  else
    return ssm_load(name) || serviceset_start(ss, name);
}

int
cmd_start(char *name) {
  int err = ssm_start(name);
  return mq_senderrmsg(out, err), err;
}

int
ssm_stop(char *name) {
  return serviceset_stop(ss, name);
}

int
cmd_stop(char *name) {
  int err = ssm_stop(name);
  return mq_senderrmsg(out, err), err;
}

int
ssm_restart(char *name) {
  return serviceset_restart(ss, name);
}

int
cmd_restart(char *name) {
  int err = ssm_restart(name);
  return mq_senderrmsg(out, err), err;
}

int
ssm_term(void) {
  serviceset_stop_all(ss);
  term = 1;
  return 0;
}

int
cmd_term(char *ign) {
  int err = ssm_term();
  return mq_senderrmsg(out, err), err;
}

int
cmd_status(char *name) {
  service *sv = serviceset_service_by_name(ss, name);
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  ser_cstr(&buf, &nbuf, &mbuf, "Err");
  ser_long(&buf, &nbuf, &mbuf, sv ? 0 : 1);
  if (sv)
    service_ser(sv, &buf, &nbuf, &mbuf);
  mq_append_msg(out, 0, buf, nbuf, mbuf);
  return mq_send(out);
}

int
cmd_list(char *ign) {
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  ser_cstr(&buf, &nbuf, &mbuf, "Err");
  ser_long(&buf, &nbuf, &mbuf, 0);
  serviceset_ser(ss, &buf, &nbuf, &mbuf);
  mq_append_msg(out, 0, buf, nbuf, mbuf);
  return mq_send(out);
}

char *
null_to_printable(char *in, size_t nin) {
  char *buf = emalloc(nin);
  for (size_t i = 0; i < nin; i++)
    if (!in[i] && i != nin - 1)
      buf[i] = '|';
    else
      buf[i] = in[i];
  return buf;
}

void
exec_cmd(char *cmd, size_t ncmd) {
  if (!cmd || !ncmd)
    return;
  char *pstr = null_to_printable(cmd, ncmd);
  logprintf(2, "exec cmd %s\n", pstr);
  free(pstr);
  for (size_t i = 0; i < ncmds; i++) {
    if (strncmp(cmd, cmds[i].cname, ncmd))
      continue;
    size_t cl = strlen(cmds[i].cname);
    char *arg = cmd + cl;
    if (!*arg && cl < ncmd - 1)
      arg++;
    else
      arg = 0;
    if (!*arg)
      arg = 0;
    cmds[i].cf(arg);
    break;
  }
}
