#include "all.h"
#include "ssm.h"
#include "ssmcmd.h"

serviceset *ss = 0;
mq *in = 0;
mq *out = 0;
mq **evq = 0;
size_t nevq = 0;
size_t mevq = 0;
int term = 0;

int dochld = 0;
int doterm = 0;
int doint = 0;
int dousr1 = 0;
int dousr2 = 0;

void
onchld(int s) {
  dochld = 1;
}
void
onterm(int s) {
  doterm = 1;
}
void
onint(int s) {
  doint = 1;
}
void
onusr1(int s) {
  dousr1 = 1;
}
void
onusr2(int s) {
  dousr2 = 1;
}

int
main(int argc, char *argv[])
{
  struct timespec startup;
  clock_gettime(CLOCK_MONOTONIC, &startup);
  char *ssmcfg_path = 0;
  for (size_t i = 1; i < (size_t)argc; i++) {
    if (!strcmp(argv[i], "-v")) {
      loglevel++;
    } else if (!strcmp(argv[i], "-q")) {
      loglevel--;
    } else if (!strcmp(argv[i], "-d")) {
      if (i + 1 >= argc)
        logfatal(1, 0, "not enough arguments");
      ssmcfg_path = estrdup(argv[++i]);
    } else {
      logfatal(1, 0, "unknown argument");
    }
  }

  int fd_dn;
  if ((fd_dn = open("/dev/null", O_RDONLY)) >= 0)
    fd_move(STDIN_FILENO, fd_dn);
  else  
    fd_close(STDIN_FILENO);

  if (!ssmcfg_path && get_ssmcfg_path(getuid(), &ssmcfg_path))
    logfatal(1, 0, "ssmcfg path not found or not accessible\n");

  char *inpath = pathmcat(ssmcfg_path, "ctlin");
  int fd_in = open(inpath, O_RDONLY | O_NONBLOCK);
  /* keep unused write end open so we can keep the read end open until exit */
  int fd_in_out = open(inpath, O_WRONLY | O_NONBLOCK);
  free(inpath);
  if (fd_in < 0)
    logfatal(1, 0, "could not open in pipe\n");
  fd_coe(fd_in);
  in = mq_new(fd_in);

  char *outpath = pathmcat(ssmcfg_path, "ctlout");
  /* keep unused read end open so we can keep the write end open until exit */
  int fd_out_in = open(outpath, O_RDONLY | O_NONBLOCK);
  int fd_out = open(outpath, O_WRONLY | O_NONBLOCK);
  free(outpath);
  if (fd_out < 0)
    logfatal(1, 0, "could not open out pipe\n");
  fd_coe(fd_out);
  if (fd_lock(fd_out, 1, 1) <= 0)
    logfatal(1, 0, "could not aquire lock\n");
  out = mq_new(fd_out);

  sigset_t sis;
  sigemptyset(&sis);
  sigaddset(&sis, SIGCHLD);
  sigaddset(&sis, SIGTERM);
  sigaddset(&sis, SIGINT);
  sigaddset(&sis, SIGUSR1);
  sigaddset(&sis, SIGUSR2);
  if (sigprocmask(SIG_SETMASK, &sis, 0))
    logfatal(1, 0, "sigprocmask failed\n");
  if (   sigsethandler(SIGCHLD, onchld)
      || sigsethandler(SIGTERM, onterm)
      || sigsethandler(SIGINT,  onint )
      || sigsethandler(SIGUSR1, onusr1)
      || sigsethandler(SIGUSR2, onusr2))
    logfatal(1, 0, "setting signal handlers failed\n");
  sigset_t e;
  sigemptyset(&e);

  ss = serviceset_new(ssmcfg_path, startup);
  if (serviceset_load(ss, "default"))
    logfatal(1, 0, "invalid service loaded\n");
  serviceset_start(ss, "default");

  struct pollfd pfd_in;
  pfd_in.fd = fd_in;
  pfd_in.events = POLLIN;

  struct pollfd *fds = 0;
  size_t nfds = 0;
  size_t mfds = 0;
  list_minsize((void **)&fds, &nfds, &mfds, sizeof(struct pollfd), ss->nsvs + 1);
  list_append((void **)&fds, &nfds, &mfds, sizeof(struct pollfd), &pfd_in);
  while (1) {
    struct timespec tmo;
    struct timespec svt;
    struct timespec now;
    clock_gettime(CLOCK_MONOTONIC, &now);
    svt.tv_sec = now.tv_sec + 3600;
    svt.tv_nsec = now.tv_nsec;
    serviceset_wakeup_time(ss, &svt);
    tmo = ts_minus(svt, now);
    nfds = 1;
    fds[0].revents = 0;
    serviceset_append_fds(ss, &fds, &nfds, &mfds);
    int r = ppoll(fds, nfds, &tmo, &e);
    if (r < 0 && !(doint || doterm || dochld || dousr1 || dousr2)) {
      logprintf(0, "unknown signal or poll error\n");
    }
    if (dousr1 || dousr2) {
      logprintf(2, "sigusr1 or sigusr2 recieved\n");
      dousr1 = dousr2 = 0;
    }
    if (doint) {
      logprintf(0, "sigint recieved, hard exit\n");
      doint = 0;
      break;
    }
    if (doterm) {
      logprintf(1, "sigterm recieved, stopping services\n");
      ssm_term();
      doterm = 0;
    }
    if (dochld) {
      int wstatus;
      pid_t pid;
      while ((pid = waitpid(-1, &wstatus, WNOHANG)) > 0) {
        int status = 255;
        if (WIFEXITED(wstatus))
          status = WEXITSTATUS(wstatus);
        if (WIFSIGNALED(wstatus))
          status = 128 + WTERMSIG(wstatus);
        logprintf(2, "exit pid=%d status=%d\n", (int)pid, status);
        serviceset_handle_exit(ss, pid, status);
      }
      dochld = 0;
    }
    if (r > 0) {
      logprintf(2, "fd ready\n");
      if (fds[0].revents & POLLIN) {
        int err = mq_recv(in);
        if (err && err != EAGAIN)
          logprintf(0, "IO Error\n");
        for (size_t i = 0; i < in->nrbufs; i++)
          if (in->rbufs[i].of)
            exec_cmd(in->rbufs[i].buf, in->rbufs[i].nbuf);
        size_t n = in->nrbufs;
        for (size_t i = 0; i < n; i++)
          if (in->rbufs[i].of)
            mq_delete_msg(in, 1, n - 1 - i);
      }
      for (size_t i = 1; i < nfds; i++)
        if (fds[i].revents)
          serviceset_notified(ss, fds[i].fd, fds[i].revents);
    }
    if (!r) {
      logprintf(2, "finished timeout sec=%ld.%09ld\n", tmo.tv_sec, tmo.tv_nsec);
    }
    serviceset_do_auto_transitions(ss);
    if (term && !serviceset_all_stopped(ss))
      break;
  }
  serviceset_delete(ss, 1);
  mq_delete(in);
  mq_delete(out);
  free(ssmcfg_path);
  close(fd_in);
  close(fd_in_out);
  close(fd_out);
  close(fd_out_in);
  free(fds);
  return 0;
}
