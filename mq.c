#include "all.h"

mq *
mq_new(int fd) {
  mq *q = calloc(1, sizeof(mq));
  q->fd = fd;
  return q;
}

void
mq_delete(mq *q) {
  for (size_t i = 0; i < q->nrbufs; i++)
    free(q->rbufs[i].buf);
  free(q->rbufs);
  for (size_t i = 0; i < q->nwbufs; i++)
    free(q->wbufs[i].buf);
  free(q->wbufs);
  free(q);
}

void
mq_append_msg(mq *q, int r, char *buf, size_t nbuf, size_t mbuf) {
  char nl = '\n';
  list_append((void **)&buf, &nbuf, &mbuf, 1, &nl);
  msgbuf mb = {buf, nbuf, mbuf, 0};
  list_append((void **)(r ? &q->rbufs : &q->wbufs),
      r ? &q->nrbufs : &q->nwbufs, r ? &q->mrbufs : &q->mwbufs, sizeof(msgbuf), &mb);
}

void
mq_delete_msg(mq *q, int r, size_t idx) {
  char *b = r ? q->rbufs[idx].buf : q->wbufs[idx].buf;
  free(b);
  list_remove_idx((void **)(r ? &q->rbufs : &q->wbufs),
      r ? &q->nrbufs : &q->nwbufs, r ? &q->mrbufs : &q->mwbufs, sizeof(msgbuf), idx);
}

int
wbuf_write(int fd, msgbuf *wb) {
  iores r = fd_write(fd, wb->buf + wb->of, wb->nbuf - wb->of);
  wb->of += r.s;
  if (wb->of == wb->nbuf) {
    free(wb->buf);
    wb->buf = 0;
    wb->nbuf = wb->mbuf = 0;
  }
  return r.err;
}

int
mq_send(mq *q) {
  while (q->nwbufs) {
    wbuf_write(q->fd, q->wbufs + 0);
    if (q->wbufs[0].buf)
      return 1;
    mq_delete_msg(q, 0, 0);
  }
  return 0;
}

int
mq_recv(mq *q) {
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  iores r = fd_read_all(q->fd, &buf, &nbuf, &mbuf);
  size_t ms = 0;
  for (size_t i = 0; i < r.s; i++) {
    if (i != r.s - 1 && buf[i] != '\n')
      continue;
    if (!q->nrbufs || q->rbufs[q->nrbufs - 1].of) {
      msgbuf mb = {0, 0, 0, 0};
      list_append((void **)&q->rbufs, &q->nrbufs, &q->mrbufs, sizeof(msgbuf), &mb);
    }
    q->rbufs[0].of = (buf[i] == '\n' ? 1 : 0);
    if (q->rbufs[0].of)
      buf[i] = 0;
    list_append_range((void **)&q->rbufs[0].buf, &q->rbufs[0].nbuf, &q->rbufs[0].mbuf, 1, i + 1 - ms, buf + ms);
  }
  free(buf);
  return r.err;
}
