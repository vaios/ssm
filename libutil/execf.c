#include "../all.h"

pid_t
execf(char *file, char *argv[], char *chdir_path, int p, int fd_target, int *fd_source, int parentread, char *in, char *out, int *out_read)
{
  if (!chdir_path)
    return -1;
  int fd_in = -1;
  if (in) {
    char *inp = pathmcat(chdir_path, in);
    fd_in = open(inp, O_RDONLY | O_NONBLOCK);
    free(inp);
    if (fd_in < 0)
      return -1;
  }
  int fd_out = -1;
  if (out) {
    if (!out_read)
      return -1;
    char *outp = pathmcat(chdir_path, out);
    *out_read = open(outp, O_RDONLY | O_NONBLOCK);
    fd_out = open(outp, O_WRONLY | O_NONBLOCK);
    free(outp);
    if (*out_read < 0 || fd_out < 0) {
      if (fd_in >= 0)
        fd_close(fd_in);
      if (*out_read >= 0)
        fd_close(*out_read);
      if (fd_out >= 0)
        fd_close(fd_out);
      return -1;
    }
  }

  int fildes[2];
  if (fd_source && pipe(fildes) < 0)
    return -1;
  if (fd_source)
    *fd_source = fildes[parentread ? 0 : 1];
  else
    fd_target = -1;

  pid_t pid = fork();
  if (pid == -1) 
    return -1;
  if (pid) {
    if (fd_source) {
      fd_close(fildes[parentread ? 1 : 0]);
      fd_coe(fildes[parentread ? 0 : 1]);
      fd_nb(fildes[parentread ? 0 : 1]);
    }
    if (fd_in >= 0)
      fd_close(fd_in);
    if (fd_out >= 0)
      fd_close(fd_out);
    return pid;
  } else {
    if (fd_in >= 0) {
      fd_bl(fd_in);
      fd_move(STDIN_FILENO, fd_in);
    }
    if (fd_out >= 0) {
      fd_bl(fd_out);
      fd_move(STDOUT_FILENO, fd_out);
      fd_dup2(STDERR_FILENO, STDOUT_FILENO);
    }
    if (fd_source) {
      fd_close(fildes[parentread ? 0 : 1]);
      fd_move(fd_target, fildes[parentread ? 1 : 0]);
    }
    if (chdir_path)
      if (chdir(chdir_path))
        _Exit(127);
    setsid();
    sigset_t e;
    sigemptyset(&e);
    sigprocmask(SIG_SETMASK, &e, 0);
    (p ? execvp : execv)(file, argv);
    _Exit(127);
  }
  return -1;
}
