#include "../all.h"

char *
estrdup(const char *s)
{
    char *p;
    while (!(p = strdup(s)))
        sleep(1u);
    return p;
}

char *
estrndup(const char *s, size_t n)
{
    char *p;
    while (!(p = strndup(s, n)))
        sleep(1u);
    return p;
}
