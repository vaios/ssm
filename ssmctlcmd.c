#include "all.h"
#include "ssmctl.h"
#include "ssmctlcmd.h"

cmd _cmds[] = {
  {"load", cmd_load},
  {"reload", cmd_reload},
  {"start", cmd_start},
  {"stop", cmd_stop},
  {"restart", cmd_restart},
  {"term", cmd_term},
  {"status", cmd_status},
  {"pid", cmd_pid},
  {"list", cmd_list},
  {"list-timers", cmd_list_timers},
  {"enable", cmd_enable},
  {"disable", cmd_disable},
};
cmd *cmds = _cmds;
size_t ncmds = LEN(_cmds);

int
mq_sendcmdarg(mq *q, char *cmd, char *arg) {
  char *buf = 0;
  size_t nbuf = 0;
  size_t mbuf = 0;
  ser_cstr(&buf, &nbuf, &mbuf, cmd);
  if (arg)
    ser_cstr(&buf, &nbuf, &mbuf, arg);
  mq_append_msg(q, 0, buf, nbuf, mbuf);
  return mq_send(q);
}

char *
null_to_printable(char *in, size_t nin) {
  char *buf = emalloc(nin);
  for (size_t i = 0; i < nin; i++)
    if (!in[i] && i != nin - 1)
      buf[i] = '|';
    else
      buf[i] = in[i];
  return buf;
}

int
deser_err(char *buf, size_t nbuf, size_t *of) {
  char *s = 0;
  long l;
  if (deser_cstr(buf, nbuf, of, &s))
    return 1;
  if (strcmp(s, "Err"))
    return free(s), 256;
  if (deser_long(buf, nbuf, of, &l))
    return 256;
  return l;
}

int
mq_geterr(mq *q) {
  mq_poll_recv(q);
  if (doterm || doint)
    logprintf(0, "geterr: TERM");
  if (!out->nrbufs)
    return 256;
  size_t of = 0;
  int err = deser_err(out->rbufs[0].buf, out->rbufs[0].nbuf, &of);
  mq_delete_msg(q, 1, 0);
  return err;
}

int
mq_print_reply(mq *q) {
  mq_poll_recv(q);
  if (doterm || doint)
    logprintf(0, "geterr: TERM");
  if (!out->nrbufs)
    return 256;
  char *pstr = null_to_printable(out->rbufs[0].buf, out->rbufs[0].nbuf);
  logprintf(1, "recv: %s\n", pstr);
  free(pstr);
  return 1;
}

int
cmd_load(char *name) {
  if (mq_sendcmdarg(in, "load", name))
    return 1;
  return mq_geterr(out);
}

int
cmd_reload(char *name) {
  if (mq_sendcmdarg(in, "reload", name))
    return 1;
  return mq_geterr(out);
}

int
cmd_start(char *name) {
  if (mq_sendcmdarg(in, "start", name))
    return 1;
  return mq_geterr(out);
}

int
cmd_stop(char *name) {
  if (mq_sendcmdarg(in, "stop", name))
    return 1;
  return mq_geterr(out);
}

int
cmd_restart(char *name) {
  if (mq_sendcmdarg(in, "restart", name))
    return 1;
  return mq_geterr(out);
}

int
cmd_term(char *ign) {
  if (mq_sendcmdarg(in, "term", 0))
    return 1;
  return mq_geterr(out);
}

int
ssmctl_getservicestate(char *name, service **svr) {
  if (mq_sendcmdarg(in, "status", name))
    return 1;
  mq_poll_recv(out);
  if (doterm || doint)
    logprintf(0, "geterr: TERM");
  if (!out->nrbufs)
    return 256;
  char *buf = out->rbufs[0].buf;
  size_t nbuf =  out->rbufs[0].nbuf;
  size_t of = 0;
  service *sv = 0;
  if (deser_err(buf, nbuf, &of))
    return 1;
  if (service_deser(buf, nbuf, &of, &sv))
    return 1;
  mq_delete_msg(out, 1, 0);
  *svr = sv;
  return 0;
}

int
cmd_status(char *name) {
  service *sv = 0;
  int r = ssmctl_getservicestate(name, &sv);
  if (r)
    return r;
  service_print_info(sv);
  service_delete(sv, 0);
  return 0;
}

int
cmd_pid(char *name) {
  service *sv = 0;
  int r = ssmctl_getservicestate(name, &sv);
  if (r)
    return r;
  if (sv->pid >= 0)
    printf("%ld\n", (long)sv->pid);
  service_delete(sv, 0);
  return 0;
}

int
ssmctl_getserviceset(serviceset **ssr) {
  if (mq_sendcmdarg(in, "list", 0))
    return 1;
  mq_poll_recv(out);
  if (doterm || doint)
    logprintf(0, "geterr: TERM");
  if (!out->nrbufs)
    return 256;
  char *buf = out->rbufs[0].buf;
  size_t nbuf =  out->rbufs[0].nbuf;
  size_t of = 0;
  serviceset *ss = 0;
  if (deser_err(buf, nbuf, &of))
    return 1;
  if (serviceset_deser(buf, nbuf, &of, &ss))
    return 1;
  *ssr = ss;
  return 0;
}

int cmd_list(char *ign) {
  serviceset *ss;
  int r = ssmctl_getserviceset(&ss);
  if (r)
    return r;
  serviceset_print_info(ss);
  serviceset_delete(ss, 0);
  mq_delete_msg(out, 1, 0);
  return 0;
}

int cmd_list_timers(char *ign) {
  serviceset *ss;
  int r = ssmctl_getserviceset(&ss);
  if (r)
    return r;
  serviceset_print_timers(ss);
  serviceset_delete(ss, 0);
  mq_delete_msg(out, 1, 0);
  return 0;
}

int
ssm_readdeps(char ***deps, size_t *ndeps, size_t *mdeps) {
  char *fp = pathmcat(ssmcfg_path, "default/dependencies");
  FILE *f = fopen(fp, "r");
  free(fp);
  if (!f)
    return (errno == ENOENT ? -1 : 1);
  if (read_linearray(f, deps, ndeps, mdeps))
    return fclose(f), 1;
  return fclose(f), 0;
}

int
ssm_writedeps(char **deps, size_t ndeps, size_t mdeps) {
  char *fp = pathmcat(ssmcfg_path, "default/dependencies");
  char *fp_new = pathmcat(ssmcfg_path, "default/dependencies_new");
  FILE *f = fopen(fp_new, "w+");
  if (!f)
    return free(fp), free(fp_new), 1;
  write_linearray(f, deps, ndeps, mdeps);
  if (fclose(f))
    return free(fp), free(fp_new), 1;
  if (unlink(fp))
    return free(fp), free(fp_new), 1;
  if (rename(fp_new, fp))
    return free(fp), free(fp_new), 1;
  return free(fp), free(fp_new), 0;
}

static ssize_t
finddep(char **deps, size_t ndeps, char *name) {
  for (size_t i = 0; i < ndeps; i++)
    if (!strcmp(deps[i], name))
      return i;
  return -1;
}

int
ssm_set_enabled(char *name, int enable) {
  if (!name)
    return 1;
  char **deps = 0;
  size_t ndeps = 0;
  size_t mdeps = 0;
  if (ssm_readdeps(&deps, &ndeps, &mdeps) > 0)
    return freestrarray(deps, ndeps, mdeps), 1;
  ssize_t idx = finddep(deps, ndeps, name);
  if (!enable && idx < 0)
    return freestrarray(deps, ndeps, mdeps), 0;
  if (enable && idx >= 0)
    return freestrarray(deps, ndeps, mdeps), 0;
  if (!enable && idx >= 0)
    list_remove_idx((void **)&deps, &ndeps, &mdeps, sizeof(char *), idx);
  if (enable && idx < 0) {
    char *s = estrdup(name);
    list_append((void **)&deps, &ndeps, &mdeps, sizeof(char *), &s);
  }
  if (ssm_writedeps(deps, ndeps, mdeps))
    return freestrarray(deps, ndeps, mdeps), 1;
  return freestrarray(deps, ndeps, mdeps), 0;
}

int
cmd_disable(char *name) {
  return ssm_set_enabled(name, 0);
}

int
cmd_enable(char *name) {
  return ssm_set_enabled(name, 1);
}

void
send_cmd(char *cmd, char *arg) {
  if (!cmd)
    return;
  for (size_t i = 0; i < ncmds; i++) {
    if (strcmp(cmd, cmds[i].cname))
      continue;
    cmds[i].cf(arg);
    return;
  }
  logfatal(1, 0, "invalid command");
}
