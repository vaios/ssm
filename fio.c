#include "all.h"

ssize_t
mgetline(char **buff, size_t *n, FILE *f) {
  *n = 0;
  *buff = 0;
  ssize_t r = getline(buff, n, f);
  if (r < 0)
    free(*buff), *buff = 0;
  if (r > 0 && (*buff)[r - 1] == '\n')
    (*buff)[r - 1] = 0;
  return r;
}

int rstrtol(const char *s, long *l) {
  char *end;
  *l = strtol(s, &end, 0);
  return s == end;
}

int
read_linearray(FILE *f, char ***argv, size_t *nargv, size_t *margv) {
  char *buff;
  size_t n;
  errno = 0;
  while (mgetline(&buff, &n, f) >= 0)
    list_append((void **)argv, nargv, margv, sizeof(char *), &buff);
  if (errno)
    return 1;
  return 0;
}

void
freestrarray(char **deps, size_t ndeps, size_t mdeps) {
  for (size_t i = 0; i < ndeps; i++)
    free(deps[i]);
  free(deps);
}


int
write_linearray(FILE *f, char **argv, size_t nargv, size_t margv) {
  for (size_t i = 0; i < nargv; i++) {
		fwrite(argv[i], 1, strlen(argv[i]), f);
		if (ferror(f))
			return 1;
    char nl = '\n';
		fwrite(&nl, 1, 1, f);
		if (ferror(f))
			return 1;
  }
  return 0;
}

int
read_all_file(FILE *f, char **buf, size_t *nbuf, size_t *mbuf) {
  size_t n = 0;
  do {
    *nbuf += n;
    if (ferror(f))
      return 1;
    if (feof(f))
      return 0;
    list_minsize((void **)buf, nbuf, mbuf, 1, *nbuf + 128);
  } while ((n = fread(*buf + *nbuf, 1, *mbuf - *nbuf, f)));
  if (ferror(f))
    return 1;
  return 0;
}

int
read_long(FILE *f, long *l) {
  char *buff;
  size_t n;
  int r;
  long t;
  if (mgetline(&buff, &n, f) < 0)
    return 1;
  if (!(r = rstrtol(buff, &t)))
    *l = t;
  free(buff);
  return r;
}
