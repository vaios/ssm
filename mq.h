typedef struct {
  char *buf;
  size_t nbuf;
  size_t mbuf;
  size_t of; /* write buffers: index of next byte we need to write
                read buffers: 0 = message incomplete */
} msgbuf;

typedef struct {
  int fd;
  msgbuf *wbufs;
  size_t nwbufs;
  size_t mwbufs;
  msgbuf *rbufs;
  size_t nrbufs;
  size_t mrbufs;
} mq;

mq *mq_new(int fd);
void mq_delete(mq *q);
void mq_append_msg(mq *q, int r, char *buf, size_t nbuf, size_t mbuf);
void mq_delete_msg(mq *q, int r, size_t idx);
int mq_send(mq *q);
int mq_recv(mq *q);
